#!/bin/bash

marge --gitlab-url=https://gitlab.freedesktop.org \
      --impersonate-approvers \
      --ci-timeout 60m \
      --add-part-of \
      --merge-order updated_at \
      --project-regexp $MARGE_PROJECT_REGEX
